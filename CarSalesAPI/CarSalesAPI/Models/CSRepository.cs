﻿using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Newtonsoft.Json;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;


namespace CarSalesAPI.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class CSRepository : ICSRepository
    {
        /// <summary>
        /// returns the car for the passed ID
        /// </summary>
        /// <param name="CarId"></param>
        /// <returns></returns>
        public Car RetrieveCar(int CarId)
        {
            List<Car> cars = this.RetrieveCar();
            Car car = cars.Find(c => c.CarId == CarId);
            return car;
        }

        /// <summary>
        /// Retrieves the list of Cars.
        /// </summary>
        /// <returns></returns>
        public List<Car> RetrieveCar()
        {
            //var filePath = HostingEnvironment.MapPath(@"~/App_Data/product.json");
            var deserializeSetting = new JsonSerializerSettings()
            {
                DateFormatString = "dd/MM/yyyy"
            };
            var filePath = "App_Data/cars.json";
            var json = System.IO.File.ReadAllText(filePath);
            var cars = JsonConvert.DeserializeObject<List<Car>>(json, deserializeSetting);
            return cars;
        }

        /// <summary>
        /// Saves a new car.
        /// </summary>
        /// <param name="car"></param>
        /// <returns></returns>
        public Car SaveCar(Car car)
        {
            // Read in the existing cars
            var cars = this.RetrieveCar();

            // Assign a new Id
            var maxId = cars.Max(c => c.CarId);
            car.CarId = maxId + 1;
            car.CreatedDate = DateTime.Now;
            cars.Add(car);

            WriteCarData(cars);
            return car;
        }

        /// <summary>
        /// updates the status to delete
        /// </summary>
        /// <param name="id"></param>
        public Car DeleteCar(int id)
        {
            Car car = this.RetrieveCar(id);
            if (car != null)
            {
                car.Status = 2;
                this.SaveCar(id, car);
            }
            return car;
        }

        /// <summary>
        /// Updates an existing Car
        /// </summary>
        /// <param name="id"></param>
        /// <param name="car"></param>
        /// <returns></returns>
        public Car SaveCar(int id, Car car)
        {
            // Read in the existing cars
            var cars = this.RetrieveCar();
            car.LastUpdatedDate = DateTime.Now;

            // Locate and replace the item
            var itemIndex = cars.FindIndex(c => c.CarId == car.CarId);
            if (itemIndex > 0)
            {
                cars[itemIndex] = car;
            }
            else
            {
                return null;
            }

            WriteCarData(cars);
            return car;
        }

        private bool WriteCarData(List<Car> cars)
        {
            var settings = new JsonSerializerSettings()
            {
                DateFormatString = "dd/MM/yyyy"
            };
            // Write out the Json
            var filePath ="App_Data/cars.json";

            var json = JsonConvert.SerializeObject(cars, Formatting.Indented, settings);
            System.IO.File.WriteAllText(filePath, json);

            return true;
        }



        /// <summary>
        /// Retrieves the list of Dealer.
        /// </summary>
        /// <returns></returns>
        public List<Dealer> RetrieveDealer()
        {
            var deserializeSetting = new JsonSerializerSettings()
            {
                DateFormatString = "dd/MM/yyyy"
            };
            var filePath = "App_Data/dealers.json";
            var json = System.IO.File.ReadAllText(filePath);
            var dealers = JsonConvert.DeserializeObject<List<Dealer>>(json, deserializeSetting);
            return dealers;
        }

        /// <summary>
        /// returns the Dealer for the passed ID
        /// </summary>
        /// <param name="dealerId"></param>
        /// <returns></returns>
        public Dealer RetrieveDealer(int dealerId)
        {
            List<Dealer> cars = this.RetrieveDealer();
            Dealer dealer = cars.Find(c => c.DealerId == dealerId);
            return dealer;
        }

        public Dealer DeleteDealer(int id)
        {
            Dealer dealer = this.RetrieveDealer(id);
            if (dealer != null)
            {
                dealer.Status = 2;
                this.SaveDealer(id, dealer);
            }
            return dealer;
        }

        /// <summary>
        /// Saves a new Dealer.
        /// </summary>
        /// <param name="dealer"></param>
        /// <returns></returns>
        public Dealer SaveDealer(Dealer dealer)
        {
            // Read in the existing Dealer
            var dealers = this.RetrieveDealer();

            // Assign a new Id
            var maxId = dealers.Max(c => c.DealerId);
            dealer.DealerId = maxId + 1;
            dealers.Add(dealer);

            WriteDealerData(dealers);
            return dealer;
        }

        /// <summary>
        /// Updates an existing Dealer
        /// </summary>
        /// <param name="id"></param>
        /// <param name="dealer"></param>
        /// <returns></returns>
        public Dealer SaveDealer(int id, Dealer dealer)
        {
            // Read in the existing Dealer
            var dealers = this.RetrieveDealer();

            // Locate and replace the item
            var itemIndex = dealers.FindIndex(c => c.DealerId == dealer.DealerId);
            if (itemIndex > 0)
            {
                dealers[itemIndex] = dealer;
            }
            else
            {
                return null;
            }

            WriteDealerData(dealers);
            return dealer;
        }

        private bool WriteDealerData(List<Dealer> dealers)
        {
            var settings = new JsonSerializerSettings()
            {
                DateFormatString = "dd/MM/yyyy"
            };
            // Write out the Json
            var filePath = "App_Data/dealers.json";

            var json = JsonConvert.SerializeObject(dealers, Formatting.Indented, settings);
            System.IO.File.WriteAllText(filePath, json);

            return true;
        }

        /// <summary>
        /// Generate and send report
        /// </summary>
        /// <param name="DealerId"></param>
        /// <param name="byCreatedDate"></param>
        /// <param name="byArchivedDate"></param>
        /// <param name="days"></param>
        /// <returns></returns>
        public bool ReportRequest(int DealerId, int byCreatedDate, int byArchivedDate, int days)
        {
            //call the report module to generate report as batch process
            return true;
        }


        /// <summary>
        /// Return results based on the parameters sent
        /// </summary>
        /// <param name="dealer"></param>
        /// <param name="car"></param>
        /// <returns></returns>
        public List<Car> Search(string dealer, SearchableCar car)
        {
            List<Car> cars = this.RetrieveCar();
            //Logic Here to serach cars based on Dealer name and Car details provided
            // TODO
            return cars;
        }

    }



    /// <summary>
    /// Filter calss will be used to customize documenting
    /// </summary>
    public class RemoveAutoGeneratedFieldFilter : IOperationFilter
    {
        public void Apply(Operation operation, OperationFilterContext context)
        {
            }
    }
}
