﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarSalesAPI.Models
{
    public class Dealer
    {
        /// <summary>
        /// Auto Generated
        /// </summary>
        public int DealerId { get; set; }

        /// <summary>
        /// mandatory
        /// </summary>
        [Required(ErrorMessage = "Name is required", AllowEmptyStrings = false)]
        public string Name { get; set; }

        /// <summary>
        /// mandatory
        /// </summary>
        [Required(ErrorMessage = "Email is required", AllowEmptyStrings = false)]
        public string Email { get; set; }

        public string Address { get; set; }

        /// <summary>
        /// Auto Generated
        /// 0 - Active (Default)
        /// 2 - Deleted
        /// </summary>
        public int Status { get; set; }
    }


}
