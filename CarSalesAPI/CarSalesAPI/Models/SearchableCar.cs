﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarSalesAPI.Models
{
    public class SearchableCar
    {
        public int Year { get; set; }

        public string Make { get; set; }

        public string Model { get; set; }

        public string Badge { get; set; }

        public string EngineSize { get; set; }

        public string Transmission { get; set; }

    }

}
