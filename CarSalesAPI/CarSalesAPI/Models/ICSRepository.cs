﻿using System.Collections.Generic;

namespace CarSalesAPI.Models
{
    public interface ICSRepository
    {
        List<Car> RetrieveCar();
        Car RetrieveCar(int carId);
        List<Dealer> RetrieveDealer();
        Dealer RetrieveDealer(int dealerId);
        Car SaveCar(Car car);
        Car SaveCar(int id, Car car);
        Dealer SaveDealer(Dealer dealer);
        Dealer SaveDealer(int id, Dealer dealer);
        Car DeleteCar(int id);
        Dealer DeleteDealer(int id);
        bool ReportRequest(int DealerId, int byCreatedDate, int byArchivedDate, int days);
        List<Car> Search(string dealer, SearchableCar car);
    }
}