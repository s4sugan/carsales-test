﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarSalesAPI.Models
{
    public class Car
    {
        /// <summary>
        /// mandatory
        /// </summary>
        [Required(ErrorMessage = "Car Year Made is required", AllowEmptyStrings = false)]
        public int Year { get; set; }

        /// <summary>
        /// mandatory
        /// </summary>
        [Required(ErrorMessage = "Car Make is required", AllowEmptyStrings = false)]
        public string Make { get; set; }

        /// <summary>
        /// mandatory
        /// </summary>
       [Required(ErrorMessage = "Car Model is required", AllowEmptyStrings = false)]
        public string Model { get; set; }

        public string Badge { get; set; }

        public string EngineSize { get; set; }

        /// <summary>
        /// mandatory
        /// </summary>
        [Required(ErrorMessage = "Car Transmission is required", AllowEmptyStrings = false)]
        public string Transmission { get; set; }

        /// <summary>
        /// mandatory
        /// </summary>
        [Required()]
        public int DealerId { get; set; }

        /// <summary>
        /// Auto Generated
        /// </summary>
        public int CarId { get; set; }

        /// <summary>
        /// Created Date - Auto Generated
        /// </summary>
        public DateTime CreatedDate { get; set;}

        /// <summary> 
        /// Archived Date  - Auto Generated
        /// </summary>
        public DateTime LastUpdatedDate { get; set; }

        /// <summary>
        /// Not Mandotory when Active - Auto Generated
        /// 0 - Active (Default)
        /// 1 - Archived
        /// 2 - Deleted
        /// </summary>
        public int Status { get; set; }
    }

}
