﻿using System;
using Microsoft.AspNetCore.Mvc;
using CarSalesAPI.Models;
using System.Net;

namespace CarSalesAPI.Controllers
{
    [Route("api/[controller]")]
    public class DealersController : Controller
    {
        private ICSRepository csrepository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="repo"></param>
        public DealersController(ICSRepository repo)
        {
            this.csrepository = repo;
        }

        /// <summary>
        /// Test GET
        /// </summary>
        /// <returns></returns>
        // GET api/values
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(csrepository.RetrieveDealer());
            }
            catch (Exception exp)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, exp);
            }
        }

        // GET api/dealers/5
        /// <remarks>
        /// Note that the key is dealer id.
        /// </remarks>
        /// <response code="200">Returns found Dealer</response>
        /// <response code="404">If the item is null</response>
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                Dealer dealer = csrepository.RetrieveDealer(id);
                if (dealer == null)
                    return NotFound("Dealer Not found");
                return Ok(dealer);
            }
            catch (Exception exp)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, exp);
            }
        }


        // POST api/dealers
        /// <remarks>
        /// Note that the key is generated when saving new Dealer.
        ///  
        ///     POST 
        ///     {
        ///        "DealerID": "Auto Generated"
        ///        "Status": " 0 - default is active and not mandatory" 
        ///     }
        /// 
        /// </remarks>
        /// <response code="200">Returns the newly created Dealer</response>
        /// <response code="404">If the item is null</response>
        [HttpPost]
        public IActionResult Post([FromBody]Dealer dealer)
        {
            if (dealer==null)
                return BadRequest("Dealer cannot be null");
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            try
            {
                return Ok(csrepository.SaveDealer(dealer));
            }
            catch (Exception exp)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, exp);
            }
        }

        // PUT api/dealers/5
        /// <remarks>
        /// Note that the key will be used to locate the Dealer to update.
        ///  
        ///     PUT 
        ///     {
        ///        "Status": " 0 - default is active and not mandatory" 
        ///     }
        /// 
        /// </remarks>
        /// <response code="201">Returns the updated Dealer</response>
        /// <response code="404">If the item is null</response>
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Dealer dealer)
        {
            if (dealer == null)
                return BadRequest("Dealer cannot be null");
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            try
            {
                return Ok(csrepository.SaveDealer(id, dealer));
            }
            catch (Exception exp)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, exp);
            }           
        }


        // PUT api/dealers/5
        /// <remarks>
        /// Note that the key will be used to locate the Dealer to update.
        ///  
        ///     DELETE 
        ///     {
        ///        "Status": " 2 - Deleted" 
        ///     }
        /// 
        /// </remarks>
        /// <response code="201">Returns the updated Dealer</response>
        /// <response code="404">If the item is null</response>
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                Dealer dealer = csrepository.RetrieveDealer(id);
                if (dealer == null)
                    return NotFound("Dealer Not found");
                return Ok(csrepository.DeleteDealer(id));
            }
            catch (Exception exp)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, exp);
            }          
        }
    }
}
