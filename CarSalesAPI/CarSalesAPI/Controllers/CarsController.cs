﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using CarSalesAPI.Models;
using System.Net;

namespace CarSalesAPI.Controllers
{
    [Route("api/[controller]")]
    public class CarsController : Controller
    {

        private ICSRepository csrepository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="repo"></param>
        public CarsController(ICSRepository repo)
        {
            this.csrepository = repo;
        }

        /// <summary>
        /// Test GET
        /// </summary>
        /// <returns></returns>
        // GET api/cars
        [HttpGet]
        public IActionResult Get()
        {
            try
            {
                return Ok(csrepository.RetrieveCar());
            }
            catch (Exception exp)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, exp);
            }
        }

        /// <summary>
        /// generates report based on passed Parameters
        /// </summary>
        /// <remarks>
        /// Report is Generated on based on the parameters
        ///        "DealerId": "Report will be email to Dealer"
        ///        "byCreatedDate": "Value 1 will generate the report based on Created Date",
        ///        "byArchivedDate": "Value 1 will generate the report based on Archived Date",
        ///        "days": "Will genrate the report based on days passed" 
        /// 
        /// </remarks>
        /// <param name="DealerId"></param>
        /// <param name="byCreatedDate"></param>
        /// <param name="byArchivedDate"></param>
        /// <param name="days"></param>
        /// <returns></returns>
        [HttpGet("Report")]
        public IActionResult GetCarReport(int DealerId, int byCreatedDate, int byArchivedDate, int days)
        {
            return Ok(csrepository.ReportRequest(DealerId,byCreatedDate,byArchivedDate,days));
        }



        // POST api/Search
        /// <remarks>
        /// Returns the search results based on the passed parameters
        ///  
        /// 
        /// </remarks>
        /// <response code="200">Returns the newly created Car</response>
        /// <response code="404">If the item is null</response>
        /// <param name="DealerName"></param>
        /// <param name="car"></param>
        /// <returns></returns>
        [HttpPost("Search")]
        public IActionResult Search(string DealerName, SearchableCar car)
        {
            return Ok(csrepository.Search(DealerName,car));
        }



        // GET api/cars/5
        /// <remarks>
        /// Note that the key is car id.
        /// </remarks>
        /// <response code="200">If the item is Found</response>
        /// <response code="404">If the item is null</response>
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                Car car = csrepository.RetrieveCar(id);
                if (car == null)
                    return NotFound("Car Not found");
                return Ok(car);
            }
            catch (Exception exp)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, exp);
            }
        }


        // POST api/cars
        /// <remarks>
        /// Note that the key is generated when saving new car.
        ///  
        ///     POST 
        ///     {
        ///        "CarID": "Auto Generated"
        ///        "CreatedDate": "Auto Updated",
        ///        "LastUpdatedTime": "Auto Updated",
        ///        "Status": " 0 - default is active and not mandatory" 
        ///     }
        /// 
        /// </remarks>
        /// <response code="200">Returns the newly created Car</response>
        /// <response code="404">If the item is null</response>
        [HttpPost]
        public IActionResult Post([FromBody]Car car)
        {
            if (car == null)
                return BadRequest("Car cannot be null");
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            try
            { 
                Car savedCar = csrepository.SaveCar(car);
                return Ok(savedCar);
            }
            catch(Exception exp)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, exp);
            }
        }


        /// <remarks>
        /// Allows Dealers to add new Car
        ///     POST 
        ///     {
        ///        "CarID": "Auto Generated"
        ///        "CreatedDate": "Auto Updated",
        ///        "LastUpdatedTime": "Auto Updated",
        ///        "Status": " 0 - default is active and not mandatory" 
        ///     }
        /// </remarks>
        /// <summary>
        /// Allows Dealers to add new Car
        /// </summary>
        /// <param name="DealerId - UserID or UserName of the Dealer"></param>
        /// <param name="car"></param>
        /// <returns></returns>
        /// <response code="200">Returns the newly created Car</response>
        /// <response code="404">If the item is null</response>
        [HttpPost("Add")]        
        public IActionResult AddCar(int DealerId, [FromBody]Car car)
        {
            if (car == null)
                return BadRequest("Car cannot be null");
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            try
            {
                car.DealerId = DealerId;
                Car savedCar = csrepository.SaveCar(car);                
                return Ok(savedCar);
            }
            catch (Exception exp)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, exp);
            }
        }


        // PUT api/cars/5
        /// <remarks>
        /// Note that the key will be used to locate the car to update.
        ///  
        ///     PUT 
        ///     {
        ///        "LastUpdatedTime": "Auto Updated",
        ///        "Status": " 0 - default is active and not mandatory" 
        ///     }
        /// 
        /// </remarks>
        /// <response code="200">Returns the updated Car</response>
        /// <response code="404">If the item is null</response>
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Car car)
        {
            if (car == null)
                return BadRequest("Car cannot be null");
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            try
            {
                return Ok(csrepository.SaveCar(id, car));
            }
            catch (Exception exp)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, exp);
            }            
        }


        // PUT api/cars/5
        /// <remarks>
        /// Note that the key will be used to locate the car to update.
        ///  
        ///     DELETE 
        ///     {
        ///        "LastUpdatedTime": "Auto Updated",
        ///        "Status": " 2 - Deleted" 
        ///     }
        /// 
        /// </remarks>
        /// <response code="200">Returns the updated Car</response>
        /// <response code="404">If the item is null</response>
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                Car car = csrepository.RetrieveCar(id);
                if (car == null)
                    return NotFound("Car Not found");
                return Ok(csrepository.DeleteCar(id));
            }
            catch (Exception exp)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, exp);
            }
        }
    }
}
